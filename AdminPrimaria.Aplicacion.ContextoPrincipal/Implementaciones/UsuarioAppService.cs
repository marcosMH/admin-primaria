﻿using AdminPrimaria.Aplicacion.ContextoPrincipal.Contratos;
using AdminPrimaria.Dominio.ContextoPrincipal.Agregados.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPrimaria.Aplicacion.ContextoPrincipal.Implementaciones
{
    public class UsuarioAppService : IUsuarioAppService
    {
        #region Variables
        private readonly IUsuarioMethods _usuarioMethods;
        #endregion

        #region Constructor
        public UsuarioAppService (IUsuarioMethods usuarioMethods)
        {
            _usuarioMethods = usuarioMethods;
        }
        #endregion

        #region Metodos
        public async Task<List<UsuarioReadOnly>> getUsers(UsuarioReadOnly filtro)
        {
            return await _usuarioMethods.getUsers(filtro);
        }
        #endregion
    }
}
