﻿using AdminPrimaria.Dominio.ContextoPrincipal.Agregados.Usuarios;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AdminPrimaria.Aplicacion.ContextoPrincipal.Contratos
{
    public interface IUsuarioAppService
    {
        /// <summary>
        /// Obtiene los datos del usuarios por filtros
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns>UsuariosReadOnly</returns>
        Task<List<UsuarioReadOnly>> getUsers(UsuarioReadOnly filtro);
    }
}
