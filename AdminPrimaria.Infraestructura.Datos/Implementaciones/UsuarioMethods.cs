﻿using AdminPrimaria.Dominio.ContextoPrincipal.Agregados.Usuarios;
using AdminPrimaria.Infraestructura.Datos.ContextoBD;
using AdminPrimaria.Infraestructura.Datos.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPrimaria.Infraestructura.Datos.Implementaciones
{
    public class UsuarioMethods : Repository<Usuario>, IUsuarioMethods
    {
        #region Variables
        private readonly AdminPrimariaDBContext _contexto;
        #endregion

        #region Constructor
        public UsuarioMethods(AdminPrimariaDBContext unitOfWork) : base(unitOfWork)
        {
            _contexto = unitOfWork as AdminPrimariaDBContext;
        }
        #endregion

        #region Metodos publicos
        /// <summary>
        /// ejecuta getUsersLogic       
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public async Task<List<UsuarioReadOnly>> getUsers(UsuarioReadOnly filtro)
        {
            try
            {
                var json = Task.Run(() => getUsersLogic(filtro));
                List<UsuarioReadOnly> result = await json;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos privados
        
        /// <summary>
        /// Logica de users
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        private List<UsuarioReadOnly> getUsersLogic(UsuarioReadOnly filtro)
        {
            try
            {

                // var filter = _contexto.Usuario.AsQueryable().Where(attribute => attribute.nombre == filtro.nombre);

                var usuarioTable = _contexto.Usuario.AsQueryable();

                var query = (from usuario in usuarioTable
                             select new UsuarioReadOnly
                             {
                                 id = usuario.id,
                                 nombre = usuario.nombre
                             }).ToList();

                return query;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
