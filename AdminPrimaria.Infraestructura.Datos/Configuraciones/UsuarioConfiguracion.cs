﻿using AdminPrimaria.Dominio.ContextoPrincipal.Agregados.Usuarios;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AdminPrimaria.Infraestructura.Datos.Configuraciones
{
    public class UsuarioConfiguracion : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.Property(attribute => attribute.id).HasColumnType("int(11)");

            builder.Property(attribute => attribute.nombre).HasColumnType("varchar(250)");
        }
    }
}   
