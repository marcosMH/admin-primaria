﻿using AdminPrimaria.Dominio.ContextoPrincipal.Agregados.Usuarios;
using AdminPrimaria.Infraestructura.Datos.Configuraciones;
using AdminPrimaria.Infraestructura.Datos.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPrimaria.Infraestructura.Datos.ContextoBD
{
    public class AdminPrimariaDBContext : DbContext, IQueryableUnitOfWork, IDesignTimeDbContextFactory<AdminPrimariaDBContext>
    {
        #region Variables
        public string ConnectionString { get; set; }
        #endregion

        #region Constructor
        public AdminPrimariaDBContext(DbContextOptions<AdminPrimariaDBContext> options)
            : base(options)
        {
        }

        public AdminPrimariaDBContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<AdminPrimariaDBContext>();
            //optionsBuilder.UseMySQL("server=localhost;port=3306;user=root;password:12345;database=db_zeropapel");
            optionsBuilder.UseMySQL("server=localhost;port=3306;password=admin;user=admin;database=admin_primary");
            return new AdminPrimariaDBContext(optionsBuilder.Options);
        }
        #endregion

        #region Configuracion
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySQL(ConnectionString
                    , MySQLOptionsAction => MySQLOptionsAction.CommandTimeout(300) //5 min
                    );
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                // optionsBuilder.UseMySQL("server=localhost;port=3306;user=root;database=admin_primary");
            }
        }
        #endregion

        #region DbSets
        public virtual DbSet<Usuario> Usuario { get; set; }
        #endregion

        #region IQueryableUnitOfWork Metodos

        public DbSet<TEntity> CreateSet<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public void AttachSet<TEntity>(TEntity item) where TEntity : class
        {
            base.Entry<TEntity>(item).State = EntityState.Unchanged;
        }

        public void SetModified<TEntity>(TEntity item) where TEntity : class
        {
            base.Entry<TEntity>(item).State = EntityState.Modified;
        }

        public void ApplyCurrentValues<TEntity>(TEntity original, TEntity current) where TEntity : class
        {
            base.Entry<TEntity>(original).CurrentValues.SetValues(current);
        }

        public IDbContextTransaction BeginTransaction()
        {
            return Database.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public void RollbackChanges()
        {
            base.ChangeTracker.Entries().ToList().ForEach(entry => entry.State = EntityState.Unchanged);
        }

        public async Task CommitAsync()
        {
            await base.SaveChangesAsync();
        }

        public async Task<int> ExecuteCommandAsync(string sqlCommand, params object[] parameters)
        {
            // dont use
            // return await base.Database.ExecuteSqlCommandAsync(sqlCommand, parameters);
            return 0;
        }

        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UsuarioConfiguracion());
        }
    }
}
