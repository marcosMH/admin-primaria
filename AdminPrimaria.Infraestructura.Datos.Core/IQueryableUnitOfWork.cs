﻿using AdminPrimaria.Dominio.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace AdminPrimaria.Infraestructura.Datos.Core
{
    public interface IQueryableUnitOfWork: IUnitOfWork, ISql
    {
        DbSet<TEntity> CreateSet<TEntity>() where TEntity : class;

        void AttachSet<TEntity>(TEntity item) where TEntity : class;

        void SetModified<TEntity>(TEntity item) where TEntity : class;

        void ApplyCurrentValues<TEntity>(TEntity original, TEntity current) where TEntity : class;

        IDbContextTransaction BeginTransaction();
    }
}
