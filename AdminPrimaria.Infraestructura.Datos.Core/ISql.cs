﻿using System.Threading.Tasks;

namespace AdminPrimaria.Infraestructura.Datos.Core
{
    public interface ISql
    {
        Task<int> ExecuteCommandAsync(string sqlCommand, params object[] parameters);
    }
}
