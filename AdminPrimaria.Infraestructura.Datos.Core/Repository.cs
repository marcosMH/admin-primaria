﻿using AdminPrimaria.Dominio.Core;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace AdminPrimaria.Infraestructura.Datos.Core
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly IQueryableUnitOfWork _unitOfWork;

        public Repository(IQueryableUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        DbSet<TEntity> GetSet()
        {
            return _unitOfWork.CreateSet<TEntity>();
        }

        public IUnitOfWork UnitOfWork
        {
            get
            {
                return this._unitOfWork;
            }
        }

        public void Add(TEntity item)
        {
            GetSet().Add(item);
        }

        public TEntity Get(object id)
        {
            var entity = GetSet().Find(id);
            return entity;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return GetSet();
        }

        public TEntity GetAsync(object id)
        {
            return null;
        }

        public void Modify(TEntity item)
        {
            this._unitOfWork.SetModified(item);
        }

        public void Remove(TEntity item)
        {
            this._unitOfWork.AttachSet(item);

            GetSet().Remove(item);
        }

        public void Dispose()
        {
            if (this._unitOfWork != null)
                this._unitOfWork.Dispose();
        }
    }
}
