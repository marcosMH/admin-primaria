﻿using System.Collections.Generic;
using System.Threading.Tasks;


namespace AdminPrimaria.Dominio.ContextoPrincipal.Agregados.Usuarios
{
    public interface IUsuarioMethods
    {
        /// <summary>
        /// Obtiene los datos del usuarios por filtros
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns>UsuariosReadOnly</returns>
        Task<List<UsuarioReadOnly>> getUsers(UsuarioReadOnly filtro);
    }
}
