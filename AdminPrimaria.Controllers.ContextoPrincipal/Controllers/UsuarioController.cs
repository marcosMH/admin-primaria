﻿using AdminPrimaria.Aplicacion.ContextoPrincipal.Contratos;
using AdminPrimaria.Dominio.ContextoPrincipal.Agregados.Usuarios;
using AdminPrimaria.Dto.UsuarioDTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AdminPrimaria.Controllers.ContextoPrincipal.Controllers
{
    [Route("api/usuario")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        #region Campos
        private readonly IUsuarioAppService _usuarioAppService;
        #endregion


        #region Constructor 
        public UsuarioController(IUsuarioAppService usuarioAppService)
        {
            _usuarioAppService = usuarioAppService;
        }
        #endregion

        #region Metodos
        /// <summary>
        /// Obtiene la lista completa de áreas.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<UsuarioReadOnly>> Get() 
        {
            var filtro = new UsuarioReadOnly();
            List<UsuarioReadOnly> data = await _usuarioAppService.getUsers(filtro);
            return data;
        }
        #endregion
    }
}
