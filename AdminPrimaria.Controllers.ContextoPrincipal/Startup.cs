using AdminPrimaria.Aplicacion.ContextoPrincipal.Contratos;
using AdminPrimaria.Aplicacion.ContextoPrincipal.Implementaciones;
using AdminPrimaria.Dominio.ContextoPrincipal.Agregados.Usuarios;
using AdminPrimaria.Infraestructura.Datos.ContextoBD;
using AdminPrimaria.Infraestructura.Datos.Implementaciones;
using AutoWrapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using SimpleInjector;
using SimpleInjector.Integration.AspNetCore.Mvc;
using SimpleInjector.Lifestyles;


namespace AdminPrimaria.Controllers.ContextoPrincipal
{
    public class Startup
    {

        private Container container = new Container();

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            
            services.AddMvc();

            services.AddDbContextPool<AdminPrimariaDBContext>(options =>
            {
                
                //options.UseMySQL("server=localhost;port=3306;user=root;password=Jyc123+;database=db_zeropapel");
                options.UseMySQL("server=localhost;port=3306;user=admin;password=admin;database=admin_primary");
            });
            var dbContextOptions = new DbContextOptionsBuilder<AdminPrimariaDBContext>().UseMySQL("server=localhost;port=3306;user=admin;password=admin;database=admin_primary");
            services.AddSingleton(dbContextOptions);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "AdminPrimaria.Controllers.ContextoPrincipal", Version = "v1" });
            });

            IntegrateSimpleInjector(services);
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;

            });

            services.AddCors();
        }

        private void IntegrateSimpleInjector(IServiceCollection services)
        {
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IControllerActivator>(new SimpleInjectorControllerActivator(container));
            services.AddSingleton<IViewComponentActivator>(new SimpleInjectorViewComponentActivator(container));

            services.EnableSimpleInjectorCrossWiring(container);
            services.UseSimpleInjectorAspNetRequestScoping(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            InitializeContainer(app, env);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AdminPrimaria.Controllers.ContextoPrincipal v1"));
            }

            app.UseHttpsRedirection();
            app.UseApiResponseAndExceptionWrapper(new AutoWrapperOptions
            {
                ShowApiVersion = true,
                ApiVersion = "1.0",
                ShowStatusCode = true,
                UseCustomSchema = true,
                IsDebug = true
            });
            app.UseRouting();

            // global cors policy
            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true) // allow any origin
                .AllowCredentials()); // allow credentials

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseCors("CorsPolicy");
        }

        private void InitializeContainer(IApplicationBuilder app, IWebHostEnvironment env)
        {
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            container.RegisterMvcControllers(app);
            container.RegisterMvcViewComponents(app);

            // container.Register<ZeroPapelDBContext>(Lifestyle.Scoped);

            // container.RegisterSingleton(typeof(IQueryableUnitOfWork), typeof(ZeroPapelDBContext));

            // container.Register<IQueryableUnitOfWork, ZeroPapelDBContext>(Lifestyle.Scoped);

            container.RegisterInitializer<AdminPrimariaDBContext>((handler) =>
            {
                handler.ConnectionString = "server=localhost;port=3306;password=admin;user=admin;database=admin_primary";
            });


            #region Repositorios
            container.Register<IUsuarioMethods, UsuarioMethods>(Lifestyle.Scoped);
            #endregion

            #region App Services
            container.Register<IUsuarioAppService, UsuarioAppService>(Lifestyle.Scoped);
            #endregion

            // container.AutoCrossWireAspNetComponents(app);
            container.CrossWire<AdminPrimariaDBContext>(app);
        }
    }
}
