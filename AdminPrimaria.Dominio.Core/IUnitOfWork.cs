﻿using System;
using System.Threading.Tasks;

namespace AdminPrimaria.Dominio.Core
{
    public interface IUnitOfWork : IDisposable
    {
        Task CommitAsync();
        void RollbackChanges();
    }
}
