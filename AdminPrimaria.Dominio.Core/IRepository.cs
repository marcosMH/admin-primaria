﻿using System;
using System.Collections.Generic;


namespace AdminPrimaria.Dominio.Core
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        IUnitOfWork UnitOfWork { get; }

        void Add(TEntity item);

        TEntity Get(object id);

        TEntity GetAsync(object id);

        public void Modify(TEntity item);

        void Remove(TEntity item);

        IEnumerable<TEntity> GetAll();
    }
}
